import ChronoPage from "./ChronoPage/ChronoPge";
import CountDownPage from "./CountDownPage/CountDownPage";
import DigiClockPage from "./DigiClockPage/DigiClockPage";
import HomePage from "./HomePage/HomePage";

export{
    ChronoPage,
    CountDownPage,
    DigiClockPage,
    HomePage,
}