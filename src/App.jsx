

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
//importamos BrowserRouter como Router para usar Router en  App()
import { DigiClock, CountDown, Nav } from './components';
import './App.scss';
import { ChronoPage, CountDownPage, DigiClockPage, HomePage } from './Pages';



function App() {
  return (
    <Router>
      <div className="app">
        <Nav />
        <div className="header">
          <h1 >React Reloj</h1>
        </div>

        <Routes>
          <Route path='/' element={<HomePage />} />;
          <Route path='/DigiClockPage' element={<DigiClockPage />} />;
          <Route path='/CountDownPage' element={<CountDownPage />} />;
          <Route path='/ChronoPage' element={<ChronoPage />} />
        </Routes>

        {/* <DigiClock />
        <div>
          <button>Cronometro</button>;
          <button>Cuenta atras</button>
        </div> */}

        {/* <CountDown /> */}
      </div>
    </Router>
  );
}

export default App;
