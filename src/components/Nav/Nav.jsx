import React from 'react';
import { Link } from 'react-router-dom';

const Nav = () => {
    return <div>
        <Link to="/">
            <button>PAGINA PRINCIPAL</button>
        </Link>
        <Link to="/DigiClockPage">
            <button>RELOJ</button>
        </Link>
        <Link to="/CountDownPage">
            <button>CUENTA ATRAS</button>
        </Link>
        <Link to="/ChronoPage">
            <button>CRONOMETRO</button>
        </Link>
    </div>;
};

export default Nav;
