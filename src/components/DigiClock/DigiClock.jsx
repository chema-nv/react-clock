import React, { useEffect, useState } from "react";
import './DigiClock.scss'

const DigiClock = () => {

/*Por un lado tenemos que definir useState para que nos setée la hora a través de una función
, y por otro definir un useEffect que nos recoja la hora local a través de una función y
 la ejecute tanto al cargar la página como al pasar a través del intervalo de un segundo. */

  const [clockState, setClockState] = useState(); //variable de estado

  //useEfect tiene dos parametos, una funcion y cada cuanto se ejecuta la funcion []dice que una vez
  //setInterval tiene los mismos parametros que useEfect
  //setinterval le pasamos una funcion que recoje la hora y de 2ª que lo haga cada segundo
  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClockState(date.toLocaleTimeString());
      //Modificamos la variable de estado cada segundo y se renderiza
    }, 1000);
  }, []);

  
  return (
    <div className="digital-clock">
      <h2 className="showCloc">{clockState}</h2>
    </div>
  );
};

export default DigiClock;
