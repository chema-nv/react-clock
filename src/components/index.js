import DigiClock from "./DigiClock/DigiClock";
import CountDown from "./CountDown/CountDown"
import Nav from "./Nav/Nav";

export{
    DigiClock,
    CountDown,
    Nav,
}